int onBoardLED = D7;
int lux;
bool activeSunlight;

void setup() {
    pinMode(onBoardLED, OUTPUT);
}

void loop() {
    //Read LUX value
    lux = analogRead(A0);
    
    if (lux >= 1250 && activeSunlight == false){
        Particle.publish("lux_event", "active", PUBLIC);
        activeSunlight = true;
    }
    else if (lux <= 200 && activeSunlight){
        Particle.publish("lux_event", "inactive", PUBLIC);    
        activeSunlight = false;
    }
    
    delay(1000);
}